package com.winio94.game;

import java.util.HashMap;
import java.util.Map;

import org.newdawn.slick.*;
import org.newdawn.slick.state.*;
/**
 * 
 * @author Michal Winnicki
 *
 */
public class Game extends StateBasedGame {

	public static final String gameName = "Mafia!";
	public static final int menu = 0;
	public static final int play = 1;
	//public static final int weaponShop = 2;

	public Game(String gameName) {
		super(gameName); // dodanie tytulu u gory ekranu
		this.addState(new Menu(menu)); // dodajemy do gry dwa rodzaje ekranu
		this.addState(new Play(play)); // inaczej: 2 stany gry(ekran menu, ekran// rozgrywki)
		//this.addState(new Menu(weaponShop));//sklep z bronia								

	}

	// GameContainer: A generic game container that handles the game loop, fps
	// recording and managing the input system

	// inicjujemy stany gry(ekrany)
	@Override
	public void initStatesList(GameContainer gc) throws SlickException {
		this.getState(menu).init(gc, this);
		this.getState(play).init(gc, this);
		//this.getState(weaponShop).init(gc, this);
		this.enterState(menu); // ustawiamy ekran/stan poczatkowy jako menu
	}

	public static void main(String[] args) {
		// okno gry
		AppGameContainer appgc;
		// ustawienie wlasciwosci okna
		try {
			// wlasciwe tworzenie okna
			appgc = new AppGameContainer(new Game(gameName));
			// dlugosc/szerokosc/pelny ekran
			appgc.setDisplayMode(1500, 900, false);
			appgc.start();// wyswietlenie okna( cos w stylu setVisible(true))

		} catch (SlickException e) {
			e.printStackTrace();
		}
		

	}

}
