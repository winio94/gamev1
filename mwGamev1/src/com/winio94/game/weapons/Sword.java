package com.winio94.game.weapons;

/**
 * 
 * @author Michal Winnicki
 *
 */
public class Sword extends Weapon {

	private double weight;

	public Sword(double weight) {
		super(5, "res/weapons/swordUp.png");
		this.weight = weight;
	}

	public double getWeight() {
		return weight;
	}

	public void setWeight(double weight) {
		this.weight = weight;
	}

}
