package com.winio94.game.weapons;

import org.newdawn.slick.Image;

/**
 * 
 * @author Michal Winnicki
 *
 */
public abstract class Weapon {

	protected int damage;
	protected int X;
	protected int Y;
	protected String path;

	public Weapon(int damage, String path) {
		super();
		this.damage = damage;
		this.path = path;
	}

	public int getDamage() {
		return damage;
	}

	public void setDamage(int damage) {
		this.damage = damage;
	}

	public int getX() {
		return X;
	}

	public void setX(int x) {
		X = x;
	}

	public int getY() {
		return Y;
	}

	public void setY(int y) {
		Y = y;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

}
