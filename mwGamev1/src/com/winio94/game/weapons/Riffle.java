package com.winio94.game.weapons;

/**
 * 
 * @author Michal Winnicki
 *
 */
import org.newdawn.slick.Image;
import org.newdawn.slick.geom.Shape;

public class Riffle extends Weapon {

	int ammo;

	public Riffle(int ammo) {
		super(10, "res/weapons/riffle.png");
		this.ammo = ammo;
	}

	public int getAmmo() {
		return ammo;
	}

	public void setAmmo(int ammo) {
		this.ammo = ammo;
	}

}
