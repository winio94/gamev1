package com.winio94.game;

import org.lwjgl.input.Mouse;
import org.lwjgl.input.Keyboard;
import org.newdawn.slick.*;
import org.newdawn.slick.state.*;

//tworzenie widoku/ekranu/stanu :D
/**
 * 
 * @author Michal Winnicki
 *
 */
public class Menu extends BasicGameState {


	private Image playNow;
	private Image exitGame;
	

	private String mouse = "";

	private int mouseX;
	private int mouseY;

	public Menu(int state) {

	}

	// inicjalizacja
	public void init(GameContainer gc, StateBasedGame sb) throws SlickException {
		playNow = new Image("res/playNow.png");
		exitGame = new Image("res/exitGame.png");
		
	}

	// metoda, ktorej uzyjemy do rysowania obiektow na ekranie
	public void render(GameContainer gc, StateBasedGame sb, Graphics g)
			throws SlickException {
		g.drawImage(playNow, 50, 50);
		g.drawImage(exitGame, 50, 150);

		g.drawString("Welcome to winio94Land", 200, 20);

		g.drawString(mouse, 500, 50);

	}

	// aktualizacja rysowanych obiektow(animacja)
	public void update(GameContainer gc, StateBasedGame sb, int delta)
			throws SlickException {

		Input input = gc.getInput();
		mouseX = Mouse.getX();
		mouseY = Mouse.getY();

		mouse = "x = " + mouseX + ",y = " + mouseY;
		
		if (mouseX > 50
				&& mouseX < (50 + playNow.getWidth())
				&& (mouseY > (gc.getHeight() - 50 - playNow.getHeight()) && mouseY < gc
						.getHeight() - 50)) {
			if (input.isMouseButtonDown(input.MOUSE_LEFT_BUTTON))
				sb.enterState(1);
		}

		if (mouseX > 50 && mouseX < (50 + exitGame.getWidth())
				&& mouseY > (gc.getHeight() - 150 - exitGame.getHeight())
				&& mouseY < gc.getHeight() - 150) {
			if (input.isMouseButtonDown(input.MOUSE_LEFT_BUTTON))
				gc.exit();
		}

	}

	// zwraca id stanu/ekranu
	public int getID() {
		return 0;
	}

}
