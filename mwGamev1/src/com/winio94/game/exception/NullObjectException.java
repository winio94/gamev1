package com.winio94.game.exception;

/**
 * 
 * @author Michal Winnicki
 *
 */
public class NullObjectException extends NullPointerException {
	public void nullObject() {
		System.out.println("Obiekt nie moze byc rowny NULL");
	}
}
