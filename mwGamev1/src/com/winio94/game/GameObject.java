package com.winio94.game;

import java.awt.Rectangle;

import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;

import com.winio94.game.Characters.Character;
/**
 * 
 * @author Michal Winnicki
 *
 */
public abstract class GameObject {
	protected float X;
	protected float Y;
	
	protected float shiftX;
	protected float shiftY;
	
	protected int width;
	protected int height;
	protected Image img;
	//FRONT//BACK//LEFT//RIGHT
	protected Image[] spirit;


	public GameObject() {
		X = 0;
		Y = 0;
	}

	public float getX() {
		return X;
	}

	public void setX(float x) {
		X = x;
	}

	public float getY() {
		return Y;
	}

	public void setY(float y) {
		Y = y;
	}

	public int getWidth() {
		return img.getWidth();
	}

	public void setWidth() {
		this.width = img.getWidth();
	}

	public int getHeight() {
		return img.getHeight();
	}

	public void setHeight() {
		this.height = img.getHeight();
	}

	public Image getImg() {
		return img;
	}

	public void setImg(Image img) {
		this.img = img;
	}

	public Image[] getSpirit() {
		return spirit;
	}

	public void setSpirit(Image[] spirit) {
		this.spirit = spirit;
	}
	
	public Image getFrontSide() {
		return spirit[0];
	}
	
	public Image getBackSide() {
		return spirit[1];
	}
	
	public Image getLeftSide() {
		return spirit[2];
	}
	
	public Image getRightSide() {
		return spirit[3];
	}
	
	

	public Rectangle getBounds() {
		try {
			return new Rectangle((int) (X - shiftX), (int) (Y - shiftY), img.getWidth(),
					img.getHeight());
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public boolean isIntersectWithObject(GameObject obj) {
		if (this.getBounds().intersects(obj.getBounds())) {
			return true;
		} else
			return false;
	}

}
