package com.winio94.game.Characters;

import java.awt.Rectangle;

import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;

import com.winio94.game.GameObject;
import com.winio94.game.exception.NullObjectException;
import com.winio94.game.exception.UnattackableException;
import com.winio94.game.weapons.Riffle;
import com.winio94.game.weapons.Sword;
import com.winio94.game.weapons.Weapon;
/**
 * 
 * @author Michal Winnicki
 *
 */
public abstract class Character extends GameObject {

	protected int HP;// life
	protected Weapon weapon;
	// add some vehicle
	protected double speed;

	public Character() {
		super();
		HP = 100;
	}

	public int getHP() {
		return HP;
	}

	public void setHP(int hP) {
		HP = hP;
	}

	public Weapon getWeapon() {
		return weapon;
	}

	public void setWeapon(Weapon weapon) {
		this.weapon = weapon;
	}

	public double getSpeed() {
		return speed;
	}

	public void setSpeed(double speed) {
		this.speed = speed;
	}

	public void Fight(Character character, Weapon weapon)
			throws UnattackableException, NullObjectException {
		if (character == null)
			throw new NullObjectException();
		else if (character == this)
			throw new UnattackableException();
		else {
			System.out.print("You are fighting with " + character.getClass()
					+ " using ");
			if (weapon instanceof Riffle)
				System.out.println("riffle");
			else if (weapon instanceof Sword)
				System.out.println("white weapon");


		}

	}

}
