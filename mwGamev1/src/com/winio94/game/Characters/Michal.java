package com.winio94.game.Characters;

import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;

import com.winio94.game.weapons.Weapon;

//SINGLETON PATTER USED
//IT CAN BE ONLY ONE MICHAL OBLJECT ON THE MAP AT TEH SAME TIME

/**
 * 
 * @author Michal Winnicki
 *
 */
public class Michal extends Character {

	private Michal() {
		super();
		// GAMEOBJECT CLASS
		try {
			super.setImg(new Image("res/winio94Front.png"));
			Image[] allSidesOfSpirit = { new Image("res/winio94Front.png"),
					new Image("res/winio94Back.png"),
					new Image("res/winio94Left.png"),
					new Image("res/winio94Right.png") };
			super.setSpirit(allSidesOfSpirit);
		} catch (SlickException se) {
			se.printStackTrace();
		}

		// CHARACTER CLASS
		super.setWeapon(null);
		super.setSpeed(0.5);

		System.out.println("Michal Object CREATED!!!");
	}

	private static final class MichalHelper {
		private static final Michal INSTANCE = new Michal();
	}

	public static Michal getMichalInstance() {
		return MichalHelper.INSTANCE;
	}

}
