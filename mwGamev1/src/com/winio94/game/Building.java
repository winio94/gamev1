package com.winio94.game;

import org.newdawn.slick.Image;
/**
 * 
 * @author Michal Winnicki
 *
 */
public class Building extends GameObject {
	public Building(float x, float y, Image img) {
		super();
		super.setX(x);
		super.setY(y);
		super.setImg(img);
		super.setHeight();
		super.setWidth();
	}
}
