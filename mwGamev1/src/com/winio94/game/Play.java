package com.winio94.game;

import java.util.ArrayList;
import java.util.List;

import org.newdawn.slick.*;
import org.newdawn.slick.state.*;

import com.winio94.game.Characters.Michal;

//tworzenie widoku/ekranu/stanu 
/**
 * 
 * @author Michal Winnicki
 *
 */
public class Play extends BasicGameState {

	//GAMEOBJECTS LIST
	private List<GameObject> listOfObject = new ArrayList<GameObject>();
	
	// mapa
	private GameObject worldMap;
	
	
	private GameObject weaponShop;
	private GameObject mcDonald;
	//private GameObject clothesShop;

	private Michal character;// postac

	// animacja postaci
	private Animation spirit;
	private Animation movingUp;
	private Animation movingDown;
	private Animation movingLeft;
	private Animation movingRight;

	boolean quit = false;

	// czas trwania pojedynczej animacji(KLATKI)
	private int[] duration = { 200, 200 };

	// pozycja postaci
	private float positionX = 0;
	private float positionY = 0;

	// ustawienie postaci na srodku ekranu
	private float shiftX;
	private float shiftY;

	public Play(int state) {

	}

	// inicjalizacja
	public void init(GameContainer gc, StateBasedGame sb) throws SlickException {
		
		// INITIALIZATION IMAGES

		worldMap = new Building(0, 0, new Image("res/worldvol3.png"));
		weaponShop = new Building(0, 0, new Image("res/weaponShop.png"));
		mcDonald = new Building(0, 0, new Image("res/mcDonald.png"));
		//clothesShop = new Building(0, 0, new Image("res/clothesShop.png"));

		// INITIALIZATION CHARACTERS
		character = Michal.getMichalInstance();

		// SETTING INITIAL POSITIONS OF OBJECTS
		positionX = character.getX();
		positionY = character.getY();

		shiftX = positionX + gc.getWidth() / 2;
		shiftY = positionY + gc.getHeight() / 2;

		// SETTING IMAGES
		Image[] walkUp = { character.getBackSide(),
				character.getBackSide()};
		Image[] walkDown = { character.getFrontSide(),
				character.getFrontSide() };
		Image[] walkRight = { character.getRightSide(),
				character.getRightSide() };
		Image[] walkLeft = { character.getLeftSide(),
				character.getLeftSide() };

		// MAKING ANIMATION
		movingUp = new Animation(walkUp, duration, false);
		movingDown = new Animation(walkDown, duration, false);
		movingLeft = new Animation(walkLeft, duration, false);
		movingRight = new Animation(walkRight, duration, false);

		// tworzymy animacje dla naszej postaci
		// na starcie postac bedzie skierowana do nas twarza
		spirit = movingDown;

		System.out.println("wysokosc: " + worldMap.height);
		System.out.println("szerokosc: " + worldMap.width);

	}

	// metoda, ktorej uzyjemy do rysowania obiektow na ekranie
	public void render(GameContainer gc, StateBasedGame sb, Graphics g)
			throws SlickException {

		// DRAWNING MAP AND BUILDINGS
		float wX = positionX - ((worldMap.width - gc.getWidth()) / 2);
		float wY = positionY - ((worldMap.height - gc.getHeight()) / 2);
		worldMap.getImg().draw(wX, wY);
		 
		weaponShop.getImg().draw(positionX + 500, positionY + 200);
		weaponShop.shiftX = 500;
		weaponShop.shiftY = 200;
		mcDonald.getImg().draw(positionX + 700, positionY + 900);

		// DRAWNING SPIRITS
		spirit.draw(shiftX, shiftY);
		String winio94Position = "winio94X: " + positionX + ", winio94Y: "
				+ positionY;
		g.drawString(winio94Position, 300, 20);

		// menu po nacisnieciu klaiwsza escape
		if (quit == true) {
			g.drawString("Resume (R)", 250, 100);
			g.drawString("Quit Game(Q)", 250, 120);
			g.drawString("Main Menu(M)", 250, 140);

			if (quit == false) {
				g.clear();
			}
		}

		character.setX(shiftX);
		character.setY(shiftY);

		float x = weaponShop.getX();
		float Y = weaponShop.getY();
		float chX = character.getX();
		float chY = character.getY();

		if(character.isIntersectWithObject(weaponShop)) {
			g.drawString("siema", 100, 100);
		}
		
		if(character.isIntersectWithObject(mcDonald)) {
			g.drawString("dupa", 100, 100);
		}
		
	}

	// aktualizacja rysowanych obiektow(animacja)
	public void update(GameContainer gc, StateBasedGame sb, int delta)
			throws SlickException {
		Input input = gc.getInput();

		if (input.isKeyDown(input.KEY_ESCAPE)) {
			quit = true;
		}
		if (quit == true) {
			if (input.isKeyDown(input.KEY_R))
				quit = false;
			else if (input.isKeyDown(input.KEY_Q))
				gc.exit();
			else if (input.isKeyDown(input.KEY_M))
				sb.enterState(0);
		}


			
		if (input.isKeyDown(input.KEY_UP)) {// dziala
			spirit = movingUp;
			if (positionY <= worldMap.getY() + worldMap.getHeight() / 2)
				positionY += delta * .5f;
		}
		if (input.isKeyDown(input.KEY_DOWN)) {
			spirit = movingDown;
			if (positionY >= worldMap.getY() - worldMap.getHeight() / 2
					+ character.getBounds().getHeight())
				positionY -= delta * .5f;
		}
		if (input.isKeyDown(input.KEY_RIGHT)) {
			spirit = movingRight;
			if (positionX >= worldMap.getX() - worldMap.getWidth() / 2
					+ character.getBounds().getWidth())
				positionX -= delta * .5f;
		}
		if (input.isKeyDown(input.KEY_LEFT)) {// dziala
			spirit = movingLeft;
			if (positionX <= worldMap.getX() + worldMap.getWidth() / 2)
				positionX += delta * .5f;
		}


	}

	// zwraca id stanu/ekranu
	public int getID() {
		return 1;
	}

}
